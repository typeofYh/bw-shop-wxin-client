/*
 * @Author: Yh
 * @LastEditors: Yh
 */

class Request {
  constructor({timeout = 60000, header = {}, baseUrl = ''}){
    this.timeout = timeout;
    this.header = header;
    this.baseUrl = baseUrl;
  }
  forMatUrl(url, params){
    if(typeof params === 'object'){
      params = Object.keys(params).map(key => `${key}=${params[key]}`).join('&')
    }
    if(url.includes('?')){ //包含问号
      return `${url}${url.endsWith('&') ? '' : '&'}${params}`
    }
    return `${url}?${params}`
  }
  get(url = "", params = {}, config = {}){
    // {page:1,pageSize:10}  [`page=1`,`pageSize=10`].join('&')  page=1&pageSize=10
    return this.sendReqest({
      url: this.forMatUrl(url, params),  // http://www.bw.com/api/list?page=1&page=1&pageSize=10
      method: 'GET',
      config
    })
  }
  post(url = "", data = {}, config = {}){
    return this.sendReqest({
      url,  
      method: 'POST',
      data,
      config
    })
  }
  sendReqest({config, ...option}){
    const httpConfig = {
      ...option,
      url: `${(/^https?/).test(option.url) ? '' : this.baseUrl}${option.url}`,   //
      header: {
        ...this.header,
        ...(config.header || {})
      },
      timeout: this.timeout
    }
    return new Promise((resolve, reject) => {
      uni.request({
        ...httpConfig,
        success(res){
          resolve(res.data || res);
        },
        fail(error){
          console.log(error);
          reject(error);
        }
      })
    })
  }
}

const userinfo = uni.getStorageSync('userinfo');
export default new Request({
  timeout: 10000,
  baseUrl: "https://bjwz.bwie.com/mall4j",
  header: {  // 所有接口携带的公共header
    Authorization: userinfo ? `${userinfo.token_type}${userinfo.access_token}` : ''
  }
});