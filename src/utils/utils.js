/*
 * @Author: Yh
 * @LastEditors: Yh
 */
export const isH5 = process.env.VUE_APP_TYPE === "h5";

export const isWxin = process.env.VUE_APP_TYPE === "weixin";

