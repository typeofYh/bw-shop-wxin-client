/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import request from "@/utils/request"
export const login = (code) => request.post('/login', {
  principal: code
})