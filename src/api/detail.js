/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import request from "@/utils/request"
export const getDetailData = (prodId) => request.get('/prod/prodInfo',{
  prodId
})