/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import request from "@/utils/request"
export const getAddressList = (pid) => request.get('/p/area/listByPid',{
  pid
})